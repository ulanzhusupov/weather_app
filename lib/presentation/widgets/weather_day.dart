import 'package:flutter/material.dart';
import 'package:flutter_home_work_weather_app/presentation/theme/app_colors.dart';
import 'package:flutter_home_work_weather_app/presentation/theme/app_fonts.dart';
import 'package:flutter_home_work_weather_app/presentation/theme/app_icons.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class WeatherDay extends StatelessWidget {
  const WeatherDay(
      {super.key, required this.weekDay, required this.weatherIcon});

  final String weekDay;
  final Widget weatherIcon;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          weekDay,
          style: AppFonts.s20W400.copyWith(color: AppColors.white),
        ),
        Row(
          children: [
            SizedBox(child: weatherIcon),
            SizedBox(width: 66.w),
            Text(
              "10°",
              style: AppFonts.s24W400.copyWith(color: AppColors.white),
            ),
            SizedBox(width: 10.w),
            Text(
              "10°",
              style:
                  AppFonts.s24W400.copyWith(color: AppColors.greyCelciumNight),
            ),
          ],
        ),
      ],
    );
  }
}
