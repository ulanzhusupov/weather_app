import 'package:flutter/material.dart';
import 'package:flutter_home_work_weather_app/presentation/theme/app_colors.dart';
import 'package:flutter_home_work_weather_app/presentation/theme/app_fonts.dart';
import 'package:flutter_home_work_weather_app/presentation/theme/app_icons.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CityTitle extends StatelessWidget {
  const CityTitle(
      {super.key,
      required this.onPressed,
      required this.isDark,
      required this.cityName});

  final Function()? onPressed;
  final bool isDark;
  final String cityName;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        TextButton(
          style: TextButton.styleFrom(padding: EdgeInsets.zero),
          onPressed: onPressed,
          child: Container(
            width: 50.w,
            height: 50.h,
            decoration: const BoxDecoration(
              color: AppColors.white,
              shape: BoxShape.circle,
            ),
            child: isDark ? AppIcons.moonMin : AppIcons.sunMin,
          ),
        ),
        SizedBox(width: 15.w),
        Text(
          cityName,
          style: AppFonts.calistogaS36W400.copyWith(color: AppColors.white),
        ),
      ],
    );
  }
}
