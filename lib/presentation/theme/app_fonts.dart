import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';

abstract class AppFonts {
  static TextStyle calistogaS36W400 = GoogleFonts.calistoga(
    fontSize: 36.sp,
    fontWeight: FontWeight.w400,
  );

  static TextStyle s24W400 =
      GoogleFonts.roboto(fontSize: 24.sp, fontWeight: FontWeight.w400);
  static TextStyle s72W700 =
      GoogleFonts.roboto(fontSize: 72.sp, fontWeight: FontWeight.w700);
  static TextStyle s22W400 =
      GoogleFonts.roboto(fontSize: 22.sp, fontWeight: FontWeight.w400);
  static TextStyle s20W400 =
      GoogleFonts.roboto(fontSize: 20.sp, fontWeight: FontWeight.w400);
}
