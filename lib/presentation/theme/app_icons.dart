// import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

abstract class AppIcons {
  static SvgPicture moonMin = SvgPicture.asset(
    "assets/icons/crescent-moon-black.svg",
    width: 25.w,
    height: 25.h,
    fit: BoxFit.scaleDown,
  );
  static SvgPicture moonMax = SvgPicture.asset(
    "assets/icons/crescent-moon-yellow.svg",
    width: 100.w,
    height: 100.h,
    fit: BoxFit.contain,
  );
  static SvgPicture sunMax = SvgPicture.asset(
    "assets/icons/sun-1.svg",
    width: 100.w,
    height: 100.h,
    fit: BoxFit.contain,
  );
  static SvgPicture sunMin = SvgPicture.asset(
    "assets/icons/sun-2.svg",
    width: 25.w,
    height: 25.h,
    fit: BoxFit.scaleDown,
  );
  static SvgPicture cloud = SvgPicture.asset("assets/icons/cloud.svg");
  static SvgPicture cloudMax = SvgPicture.asset(
    "assets/icons/cloud.svg",
    width: 100.w,
    height: 100.h,
    fit: BoxFit.contain,
  );
  static SvgPicture rain = SvgPicture.asset("assets/icons/rain.svg");
  static SvgPicture rainMax = SvgPicture.asset(
    "assets/icons/rain.svg",
    width: 100.w,
    height: 100.h,
    fit: BoxFit.contain,
  );
  static SvgPicture snow = SvgPicture.asset("assets/icons/snowflake.svg");
  static SvgPicture snowMax = SvgPicture.asset(
    "assets/icons/snowflake.svg",
    width: 100.w,
    height: 100.h,
    fit: BoxFit.contain,
  );
}
