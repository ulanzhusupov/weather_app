import 'package:flutter/material.dart';

abstract class AppColors {
  static const LinearGradient dayMode = LinearGradient(
    colors: [
      Color(0xFF4BB5F1),
      Color(0xFF2F2CBC),
    ],
    begin: Alignment.bottomLeft,
    end: Alignment.topRight,
  );

  static const LinearGradient nightMode = LinearGradient(
    colors: [
      Color(0xFF223076),
      Color(0xFF06050E),
    ],
    begin: Alignment.bottomLeft,
    end: Alignment.topRight,
  );
  static const Color darkBlueAppBar = Color(0xFF06050E);
  static const Color lightBlueAppBar = Color(0xFF2F2CBC);
  static const Color white = Colors.white;
  static const Color greyDivider = Color(0xFF949494);
  static const Color greyCelciumNight = Color(0xFFA8A8A8);
}
