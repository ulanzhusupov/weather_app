import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_home_work_weather_app/api_key.dart';
import 'package:flutter_home_work_weather_app/core/consts/consts.dart';
import 'package:flutter_home_work_weather_app/core/network/dio_settings.dart';
import 'package:flutter_home_work_weather_app/data/models/weather_model.dart';
import 'package:flutter_home_work_weather_app/presentation/theme/app_colors.dart';
import 'package:flutter_home_work_weather_app/presentation/theme/app_fonts.dart';
import 'package:flutter_home_work_weather_app/presentation/theme/app_icons.dart';
import 'package:flutter_home_work_weather_app/presentation/widgets/city_title.dart';
import 'package:flutter_home_work_weather_app/presentation/widgets/weather_day.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({super.key});

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  String cityName = "Bishkek";
  int countryCode = 996;
  final String _apiKey = ApiKey.apiKey;

  bool isDark = DateTime.now().hour < 7 || DateTime.now().hour >= 18;
  String weatherDescription = "";
  double celcius = 0;

  List<String> months = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec'
  ];

  Map<String, Widget> weatherConditions = {
    "rain": AppIcons.rainMax,
    "clear": DateTime.now().hour < 7 || DateTime.now().hour >= 18
        ? AppIcons.moonMax
        : AppIcons.sunMax,
    "snow": AppIcons.snowMax,
    "clouds": AppIcons.cloudMax,
  };

  Future<void> getData() async {
    final Dio dio = DioSettings().dio;
    final response = await dio.get('weather?', queryParameters: {
      'lat': 42.882,
      'long': 74.582,
      'appid': AppConsts.apiKey,
      'units': 'metric',
    });

    final WeatherModel model = WeatherModel.fromJson(response.data);

    cityName = model.name ?? "";
    countryCode = 996;
    weatherDescription = model.weather?.first.main ?? "";
    celcius = model.main?.temp ?? 0.0;
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnimatedContainer(
        duration: const Duration(milliseconds: 700),
        curve: Curves.fastOutSlowIn,
        decoration: BoxDecoration(
          gradient: isDark ? AppColors.nightMode : AppColors.dayMode,
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 21.w),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: 28.h),
                CityTitle(
                  cityName: cityName,
                  isDark: isDark,
                  onPressed: () {
                    isDark = !isDark;
                    setState(() {});
                  },
                ),
                SizedBox(height: 24.h),
                Text(
                  weatherDescription != "" ? weatherDescription : "...",
                  style: AppFonts.s24W400.copyWith(color: AppColors.white),
                ),
                SizedBox(height: 24.h),
                SizedBox(
                  child: weatherConditions[weatherDescription.toLowerCase()],
                ),
                SizedBox(height: 12.h),
                Text(
                  celcius != 0 ? "$celcius°" : "...",
                  style: AppFonts.s72W700.copyWith(color: AppColors.white),
                ),
                SizedBox(height: 24.h),
                Text(
                  "${months[DateTime.now().month - 1]} ${DateTime.now().day}, ${DateTime.now().year}",
                  style: AppFonts.s22W400.copyWith(color: AppColors.white),
                ),
                SizedBox(height: 24.h),
                const Divider(color: AppColors.greyDivider),
                WeatherDay(
                  weekDay: "Monday",
                  weatherIcon: AppIcons.cloud,
                ),
                const Divider(color: AppColors.greyDivider),
                WeatherDay(
                  weekDay: "Tuesday",
                  weatherIcon: AppIcons.sunMin,
                ),
                const Divider(color: AppColors.greyDivider),
                WeatherDay(
                  weekDay: "Wednesday",
                  weatherIcon: AppIcons.sunMin,
                ),
                const Divider(color: AppColors.greyDivider),
                WeatherDay(
                  weekDay: "Thursday",
                  weatherIcon: AppIcons.rain,
                ),
                const Divider(color: AppColors.greyDivider),
                WeatherDay(
                  weekDay: "Friday",
                  weatherIcon: AppIcons.snow,
                ),
                const Divider(color: AppColors.greyDivider),
                SizedBox(height: 26.h)
              ],
            ),
          ),
        ),
      ),
    );
  }
}
